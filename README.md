# python_task

This git Repo include some basic ttask and option availables in GCC compiler.

# How to run C++ program using different flags G++

| How to                           | Flags              | Command               | Output                                  |
| -------------------------------- | ------------------ | --------------------- | --------------------------------------- |
| Creat object file                | -c                 | g++ -c main.cpp       | creat object file with extension(.o)    |
| Creat exec file from object file | -o                 | g++ -o main.o         | creat a.out exec file                   |
| Creat exec file from .cpp file   | -o                 | g++ -o main.cpp       | creat a.out exec file                   |
| view output/run program          |                    | ./a.out               |                                         |
| Warning flag                     | -w, -wall          | g++ -wall -o main.cpp | turns on compiler warnings              |
| Optimization flag                | -O0, -O1, -O2, -O3 | g++ -wall -o main.cpp | turnson different type of optimizations |

# Task-1

In Task-1 I have implemented a basic calculator functions which has (Add subtract multiply divide) in seperate files. To view output run the following command

- `cd Task_1`
- `gcc -Wall -c calc_mul.cpp calc_add.cpp calc_div.cpp calc_sub.cpp class_main.cpp`
- `gcc -Wall calc_mul.o calc_add.o calc_div.o calc_sub.o class_main.o -lstdc++ -o calculator`
- `./calculator`

# Task-2

in this task a static library of calculator is created and linked this library with in main.cpp to use its functions. To creat static library and view output of Task-2 follow these step.

- `cd Task-2`

## How to creata and use static library

First add code to your header file(mylib.h) and library file (mylib.cpp), then creat object file from mylib.cpp file

- `g++ -c mylib.cpp`
- `ar rcs mylib.a mylib.o`

Static library with name (mylib.a) is created in current working directory and if the main code (main.cpp) also present in the same directory as static library then run the following commands and view output

- `g++ -o main.cpp -L. -l_mylib`
- `./a.out`

### Include path of header and library

If the main application and static libraray exist in different directories then checkout to main application directory and include the path of header and library using these methods

- Use command line to include Path of header and library
  - `g++ -c -I./header/file/path/ main.cpp`
  - `g++ -o main main.o -L./library/path/ -l_mylib`
  - `./main`
- Use Environment Variables to include Path of header and library files
  - `LIBRARY_PATH=/Path/to/Library/file`
  - `export LIBRARY_PATH`
  - `CPLUS_INCLUDE_PATH=/Path/to/Header/file`
  - `export CPLUS_INCLUDE_PATH`
  - `g++ -c main.cpp`
  - `g++ -o main main.o -l_mylib`
  - `./main`
